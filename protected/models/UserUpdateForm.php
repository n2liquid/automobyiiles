<?php
class UserUpdateForm extends CFormModel
{
	protected $user;
	protected $login;
	protected $password;
	protected $email;
	protected $newPassword;
	protected $confirmNewPassword;

	public function attributeNames()
	{
		return array (
			'login'
			, 'password'
			, 'email'
			, 'newPassword'
			, 'confirmNewPassword'
		);
	}

	public function getUserId()
	{
		return $this->user->id;
	}

	public function requirePassword()
	{
		$sessionUser = User::model()->fromSession();
		return (!$sessionUser->isAdmin() || $sessionUser->id === $this->user->id);
	}

	public function deletionAllowed()
	{
		$sessionUser = User::model()->fromSession();
		return ($sessionUser->isAdmin() && $sessionUser->id !== $this->user->id);
	}

	public function fromUser($user)
	{
		$this->user = $user;
		$this->login = $user->attributes['login'];
		$this->password = '';
		$this->email = $user->attributes['email'];
		$this->newPassword = '';
		$this->confirmNewPassword = '';
	}

	public function fromPostData($data)
	{
		if(!empty($data['password']))
		{
			$this->password = $data['password'];
		}

		$this->email = $data['email'];
		$this->newPassword = $data['newPassword'];
		$this->confirmNewPassword = $data['confirmNewPassword'];
	}

	public function validate($attributes = null, $clearErrors = true)
	{
		parent::validate($attributes, $clearErrors);

		if(!$this->user)
		{
			throw new CException("Cannot validate user update form without a user.");
		}

		if($this->requirePassword() && !$this->user->checkPassword($this->password))
		{
			$this->addError('password', "Password is incorrect.");
		}

		if($this->newPassword !== $this->confirmNewPassword)
		{
			$this->addError('confirmNewPassword', "Passwords don't match.");
		}

		return !$this->hasErrors();
	}

	public function execute()
	{
		if(!$this->validate())
		{
			return false;
		}

		$this->user->email = $this->email;

		if(!empty($this->newPassword))
		{
			$this->user->password = $this->newPassword;
		}

		if(!$this->user->save())
		{
			Yii::log (
				"Could not save User model. Details: "
				. json_encode($user->getErrors())
				, 'error'
				, 'system.web.' . get_class($this)
			);

			throw new CHttpException(500, "Internal server error.");
		}

		return true;
	}
}
