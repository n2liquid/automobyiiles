<?php
class User extends CActiveRecord
{
	protected static $Profiles = array (
		0 => 'Admin'
		, 1 => 'Employee'
	);

	public static function GetProfileName($id)
	{
		if(isset(User::$Profiles[$id]))
		{
			return User::$Profiles[$id];
		}
		else
		{
			return null;
		}
	}

	public static function GetProfileId($profile)
	{
		$ids = array_flip(User::$Profiles);

		if(isset($ids[$profile]))
		{
			return $ids[$profile];
		}
		else
		{
			return null;
		}
	}

	public function tableName()
	{
		return 'user';
	}

	public function rules()
	{
		return array (
			array('login, email, password', 'required')
			, array('profile_id', 'numerical', 'integerOnly' => true)
			, array('login', 'length', 'max' => 30)
			, array('email', 'length', 'max' => 254)
			, array('password', 'length', 'max' => 512)
			, array('id, profile_id, login, email', 'safe', 'on' => 'search')
		);
	}

	public function relations()
	{
		return array();
	}

	public function attributeLabels()
	{
		return array (
			'id' => 'ID'
			, 'profile_id' => 'Profile'
			, 'login' => 'Login'
			, 'email' => 'Email'
			, 'password' => 'Password'
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('profile_id', $this->profile_id);
		$criteria->compare('login', $this->login, true);
		$criteria->compare('email', $this->email, true);

		return new CActiveDataProvider (
			$this, array (
				'criteria' => $criteria
			)
		);
	}

	public function fromSession()
	{
		$id = Yii::app()->user->getId();

		if($id === null)
		{
			return null;
		}

		return $this->findByPk(Yii::app()->user->getId());
	}

	public function findByLogin($login)
	{
		$criteria = new CDbCriteria();

		$criteria->addColumnCondition(array('login' => $login));
		$criteria->limit = 1;

		return $this->query($criteria);
	}

	public function isAdmin()
	{
		return (int)$this->profile_id === User::GetProfileId('Admin');
	}

	public function findByEmail($email)
	{
		$criteria = new CDbCriteria();

		$criteria->addColumnCondition(array('email' => $email));
		$criteria->limit = 1;

		return $this->query($criteria);
	}

	public function checkPassword($password)
	{
		if(!$this->id)
		{
			throw new CException("Cannot check password of empty user model.");
		}

		return password_verify($password, $this->password);
	}

	protected function beforeSave()
	{
		if(!$this->isNewRecord)
		{
			$compare = User::model()->findByPk($this->id);

			if($this->password !== $compare->password)
			{
				$this->hashPassword();
			}
		}
		else
		{
			$this->hashPassword();
		}

		return parent::beforeSave();
	}

	protected function hashPassword()
	{
		$this->password = password_hash($this->password, PASSWORD_DEFAULT);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
