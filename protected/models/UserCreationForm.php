<?php
class UserCreationForm extends CFormModel
{
	protected $profile;
	protected $login;
	protected $email;
	protected $generatedPassword;

	public function attributeNames()
	{
		return array (
			'login'
			, 'email'
			, 'profile'
		);
	}

	public function getGeneratedPassword()
	{
		return $this->generatedPassword;
	}

	public function fromPostData($data)
	{
		$this->setAttributes($data, false);
	}

	public function validate($attributes = null, $clearErrors = true)
	{
		parent::validate($attributes, $clearErrors);

		if(User::GetProfileId($this->profile) === null)
		{
			$this->addError('profile', "Invalid profile.");
		}

		$this->login = trim($this->login);

		if(empty($this->login))
		{
			$this->addError('login', "Username is required.");
		}
		else
		if(strlen($this->login) < 3)
		{
			$this->addError('login', "Username must be at least 3 characters long.");
		}
		else
		if(User::model()->findByLogin($this->login))
		{
			$this->addError('login', "This username is already in use.");
		}

		$this->email = trim($this->email);

		if(empty($this->email))
		{
			$this->addError('email', "Email is required.");
		}
		else
		if(User::model()->findByEmail($this->email))
		{
			$this->addError('email', "This email is already in use.");
		}

		return !$this->hasErrors();
	}

	public function execute()
	{
		if(!$this->validate())
		{
			return false;
		}

		$user = new User();
		$user->profile_id = User::GetProfileId($this->profile);
		$user->login = $this->login;
		$user->email = $this->email;
		$user->password = $this->generatedPassword = $this->generatePassword();

		if(!$user->save())
		{
			Yii::log (
				"Could not save User model. Details: "
				. json_encode($user->getErrors())
				, 'error'
				, 'system.web.' . get_class($this)
			);

			throw new CHttpException(500, "Internal server error.");
		}
		else
		{
			$this->profile = '';
			$this->login = '';
			$this->email = '';
		}

		return true;
	}

	protected function generatePassword()
	{
		return base_convert(uniqid(), 16, 36);
	}
}
