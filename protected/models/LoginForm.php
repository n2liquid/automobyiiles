<?php
class LoginForm extends CFormModel
{
	protected $username;
	protected $password;

	public function attributeNames()
	{
		return array('username', 'password');
	}

	public function rules()
	{
		return array (
			array('username, password', 'required')
		);
	}

	public function authenticate()
	{
		$identity = new UserIdentity($this->username, $this->password);

		if(!$identity->authenticate())
		{
			$this->addError('username', "Wrong username or password.");
			return null;
		}
		else
		{
			return $identity;
		}
	}
}
