<?php
	/* @var $this Controller */
	$app = Yii::app();
	$baseUrl = $app->request->baseUrl . '/index.php';
	$baseAssetUrl = $app->request->baseUrl;
	$sessionUser = User::model()->fromSession();
	$activeMenu = $this->activeMenu;
	$pageTitle = $app->name;
?>
<!doctype html>
<meta charset="utf-8">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo $baseAssetUrl ?>/css/main.css">
<script src="//code.jquery.com/jquery-2.1.0.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<title><?php echo CHtml::encode($pageTitle) ?></title>
<header>
	<h1>
		<a href="<?php echo $baseUrl; ?>">
			<?php echo CHtml::encode($app->name); ?>
		</a>
	</h1>
	<nav class="navbar navbar-inverse">
		<ul class="nav navbar-nav">
			<?php
				if($sessionUser)
				{
				?>
					<li>
						<a href="#!" class="dropdown-toggle" data-toggle="dropdown">
							Accounts <b class="caret"></b>
						</a>
						<ul class="dropdown-menu">
							<?php
								if($sessionUser->isAdmin())
								{
								?>
									<li>
										<a href="<?php echo $baseUrl; ?>/user/create">
											Create user
										</a>
									</li>
								<?php
								}
							?>
							<li>
								<a href="<?php echo $baseUrl; ?>/user/update/<?php echo $sessionUser->id; ?>">
									Settings
								</a>
							</li>
							<li>
								<a href="<?php echo $baseUrl; ?>/site/logout">
									Log out
								</a>
							</li>
						</ul>
					</li>
				<?php
				}
				else
				{
				?>
					<li class="<?php if($activeMenu === 'login') echo 'active'; ?>">
						<a trim href="<?php echo $baseUrl; ?>/site/login">
							Log in
						</a>
					</li>
				<?php
				}
				?>
		</ul>
	</nav>
</header>
<?php echo $content; ?>
<footer>
	Copyleft (Ↄ) <?php echo $app->params['devYears']; ?> Guilherme Prá Vieira.
	<a trim href="https://www.gnu.org/philosophy/free-sw.html">
		  All wrongs reversed.
	</a>
</footer>
