<?php
	/* @var $this SiteController */
	/* @var $model LoginForm */
	$app = Yii::app();
	$this->activeMenu = 'login';
	$baseUrl = $app->request->baseUrl;
	$errors = $model->getErrors();

	if(!empty($errors['username']))
	{
		?>
		<div class="alert alert-danger">
			<?php echo CHtml::encode($errors['username'][0]); ?>
		</div>
		<?php
	}
?>
<h1>
	Login
</h1>
<p>
	Use your login username and password to authenticate.
</p>
<form class="login-form" method="post">
	<input
		class="form-control"
		type="text"
		name="LoginForm[username]"
		value="<?php echo $model->getAttributes()['username']; ?>"
		placeholder="Username"
	>
	<input class="form-control" type="password" name="LoginForm[password]" placeholder="Password">
	<input class="btn btn-default" type="submit" value="Login">
	<span class="align-with-btn-baseline">
		<a trim class="forgot-password" href="<?php echo $baseUrl . "/site/forgot-password"; ?>">
			Forgot password?
		</a>
	</span>
</form>
