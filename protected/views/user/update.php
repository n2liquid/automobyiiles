<?php
	/* @var $this UserController */
	/* @var $model UserUpdateForm */
	/* @var $updated boolean */
	$app = Yii::app();
	$baseUrl = $app->request->baseUrl . '/index.php';
	$errors = $model->getErrors();

	if($updated)
	{
	?>
		<div class="alert alert-success">
			User information updated successfuly.
		</div>
	<?php
	}

	foreach($errors as $field_errors)
	{
		foreach($field_errors as $error)
		{
			?>
			<div class="alert alert-danger">
				<?php echo CHtml::encode($error); ?>
			</div>
			<?php
		}
	}
?>
<h1>
	Update user
</h1>
<p>
	Update user information.
</p>
<form class="user-update-form" method="post">
	<input
		class="form-control"
		type="text"
		value="<?php echo $model->attributes['login']; ?>"
		disabled
	>
	<?php
		if($model->requirePassword())
		{
		?>
			<input class="form-control" type="password" name="UserUpdate[password]" placeholder="Password">
		<?php
		}
	?>
	<input
		class="form-control"
		type="text"
		name="UserUpdate[email]"
		value="<?php echo $model->attributes['email']; ?>"
		placeholder="Email"
	>
	<input
		class="form-control"
		type="password"
		name="UserUpdate[newPassword]"
		value="<?php echo $model->attributes['newPassword']; ?>"
		placeholder="New password (Leave blank for same)"
	>
	<input
		class="form-control"
		type="password"
		name="UserUpdate[confirmNewPassword]"
		value="<?php echo $model->attributes['confirmNewPassword']; ?>"
		placeholder="Confirm new password"
	>
	<input class="btn btn-default" type="submit" value="Update">
	<?php
		if($model->deletionAllowed())
		{
		?>
			<input
				class="btn btn-danger"
				type="submit"
				onclick="
					$(this).parents('form').attr (
						'action'
						, '<?php echo $baseUrl . '/user/delete/' . $model->getUserId(); ?>'
					)
				"
				value="Delete this user"
			>
		<?php
		}
	?>
</form>
