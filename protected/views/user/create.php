<?php
	/* @var $this UserController */
	/* @var $model UserCreationForm */
	/* @var $created boolean */
	$app = Yii::app();
	$baseUrl = $app->request->baseUrl . '/index.php';
	$errors = $model->getErrors();

	if($created)
	{
	?>
		<div class="alert alert-success">
			User created successfuly (Password: <?php
				echo $model->getGeneratedPassword();
			?>.)
		</div>
	<?php
	}

	foreach($errors as $field_errors)
	{
		foreach($field_errors as $error)
		{
			?>
			<div class="alert alert-danger">
				<?php echo CHtml::encode($error); ?>
			</div>
			<?php
		}
	}
?>
<h1>
	Create user
</h1>
<p>
	Create a new user.
</p>
<form class="user-creation-form" method="post">
	<select
		class="form-control"
		name="UserCreation[profile]"
	>
		<option>Admin</option>
		<option selected>Employee</option>
	</select>
	<input
		class="form-control"
		type="text"
		name="UserCreation[login]"
		value="<?php echo $model->attributes['login']; ?>"
		placeholder="Username"
		required
	>
	<input
		class="form-control"
		type="text"
		name="UserCreation[email]"
		value="<?php echo $model->attributes['email']; ?>"
		placeholder="Email"
		required
	>
	<input class="btn btn-default" type="submit" value="Create">
</form>
