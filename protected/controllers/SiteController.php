<?php

class SiteController extends Controller
{
	protected $activeMenu;

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	public function actionLogin()
	{
		$app = Yii::app();
		$model = new LoginForm();

		if(!empty($_POST['LoginForm']))
		{
			$model->attributes = $_POST['LoginForm'];
			$identity = $model->authenticate();

			if($identity)
			{
				$app->user->login($identity, 0);
				$this->redirect($app->user->returnUrl);
			}
		}

		$this->render('login', array("model" => $model));
	}

	public function actionLogout()
	{
		$app = Yii::app();
		$app->user->logout();
		$this->redirect($app->user->returnUrl);
	}
}
