<?php
class UserController extends Controller
{
	protected $activeMenu;

	public function filters()
	{
		return array (
			'accessControl'
			, 'postOnly + delete'
		);
	}

	public function accessRules()
	{
		return array (
			array (
				'allow'
				, 'actions' => array('create', 'update', 'delete')
				, 'users' => array('@')
			)
			, array (
				'deny'
				, 'users' => array('*')
			)
		);
	}

	public function actionCreate()
	{
		$this->requireUserProfile('Admin');

		$created = false;

		$creationForm = new UserCreationForm();

		if(isset($_POST['UserCreation']))
		{
			$creationForm->fromPostData($_POST['UserCreation']);
			$created = $creationForm->execute();
		}

		$this->render (
			'create', array (
				'model' => $creationForm
				, 'created' => $created
			)
		);
	}

	public function actionUpdate($id)
	{
		if(Yii::app()->user->getId() !== $id)
		{
			$this->requireUserProfile('Admin');
		}

		$updated = false;

		$targetUser = $this->loadModel($id);

		$updateForm = new UserUpdateForm();
		$updateForm->fromUser($targetUser);

		if(isset($_POST['UserUpdate']))
		{
			$updateForm->fromPostData($_POST['UserUpdate']);
			$updated = $updateForm->execute();
		}

		$this->render (
			'update', array (
				'model' => $updateForm
				, 'updated' => $updated
			)
		);
	}

	public function actionDelete($id)
	{
		$this->requireUserProfile('Admin');

		$sessionUser = User::model()->fromSession();

		if($id === $sessionUser->id)
		{
			throw new CHttpException(403, "You are not allowed to delete your own administrative account.");
		}

		$targetUser = $this->loadModel($id);

		if(!$targetUser->delete())
		{
			Yii::log (
				"Could not delete User model. Details: "
				. json_encode($targetUser->getErrors())
				, 'error'
				, 'system.web.' . get_class($this)
			);

			throw new CHttpException(500, "Internal server error.");
		}

		$this->render('deleted');
	}

	protected function requireUserProfile($profile)
	{
		$profile_id = User::GetProfileId($profile);

		$sessionUser = User::model()->fromSession();

		if($sessionUser && (int)$sessionUser['profile_id'] !== $profile_id)
		{
			throw new CHttpException(403, "You are not authorized to perform this action.");
		}
	}

	public function loadModel($id)
	{
		$model = User::model()->findByPk($id);

		if($model === null)
		{
			throw new CHttpException(404, "The requested page does not exist.");
		}

		return $model;
	}
}
