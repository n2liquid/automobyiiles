<?php
class UserIdentity extends CUserIdentity
{
	protected $id;

	public function getId()
	{
		return $this->id;
	}

	public function authenticate()
	{
		$user = User::model()->findByLogin($this->username);

		if($user && $user->checkPassword($this->password))
		{
			$this->id = $user->id;
			return true;
		}
		else
		{
			return false;
		}
	}
}
